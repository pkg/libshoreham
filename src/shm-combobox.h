/*
 * Copyright © 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __SHM_COMBO_BOX_H_
#define __SHM_COMBO_BOX_H_

#include <glib-object.h>
#include <glib.h>
#include <clutter/clutter.h>
#include <math.h>
#include <string.h>
#include "shoreham.h"

G_BEGIN_DECLS

#define SHM_TYPE_COMBO_BOX shm_combo_box_get_type ()

#define SHM_COMBO_BOX(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), SHM_TYPE_COMBO_BOX, ShmComboBox))
#define SHM_COMBO_BOX_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  SHM_TYPE_COMBO_BOX, ShmComboBoxClass))
#define SHM_IS_COMBO_BOX(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), SHM_TYPE_COMBO_BOX))
#define SHM_IS_COMBO_BOX_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  SHM_TYPE_COMBO_BOX))
#define SHM_COMBO_BOX_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  SHM_TYPE_COMBO_BOX, ShmComboBoxClass))

typedef struct _ShmComboBox ShmComboBox;
typedef struct _ShmComboBoxClass ShmComboBoxClass;
typedef struct _ShmComboBoxPrivate ShmComboBoxPrivate;

struct _ShmComboBox
{
  GObject parent;

  ShmComboBoxPrivate *priv;
};

/************************************************************************************************************/
#define SHM_COMBO_BOX_PLUGIN_DECLARE(ObjectName, object_name)           \
  static GType g_define_type_id = 0;                                    \
                                                                        \
  /* Prototypes */                                                      \
  G_MODULE_EXPORT                                                       \
  GType object_name##_get_type (void);                                  \
                                                                        \
  G_MODULE_EXPORT                                                       \
  GType object_name##_register_type (GTypeModule *type_module);         \
                                                                        \
  G_MODULE_EXPORT                                                       \
  GType shm_combo_box_plugin_register_type (GTypeModule *type_module);  \
                                                                        \
  GType                                                                 \
  object_name##_get_type (void)                                         \
  {                                                                     \
    return g_define_type_id;                                            \
  }                                                                     \
                                                                        \
  static void object_name##_init (ObjectName *self);                    \
  static void object_name##_class_init (ObjectName##Class *klass);      \
  static gpointer object_name##_parent_class = NULL;                    \
  static void object_name##_class_intern_init (gpointer klass)          \
  {                                                                     \
    object_name##_parent_class = g_type_class_peek_parent (klass);      \
    object_name##_class_init ((ObjectName##Class *) klass);             \
  }                                                                     \
                                                                        \
  GType                                                                 \
  object_name##_register_type (GTypeModule *type_module)                \
  {                                                                     \
    static const GTypeInfo our_info =                                   \
      {                                                                 \
        sizeof (ObjectName##Class),                                     \
        NULL, /* base_init */                                           \
        NULL, /* base_finalize */                                       \
        (GClassInitFunc) object_name##_class_intern_init,               \
        NULL,                                                           \
        NULL, /* class_data */                                          \
        sizeof (ObjectName),                                            \
        0, /* n_preallocs */                                            \
        (GInstanceInitFunc) object_name##_init                          \
      };                                                                \
                                                                        \
    g_define_type_id = g_type_module_register_type (type_module,        \
                                                    SHM_TYPE_COMBO_BOX, \
                                                    #ObjectName,        \
                                                    &our_info,          \
                                                    0);                 \
                                                                        \
    return g_define_type_id;                                            \
  }                                                                     \
                                                                        \
  G_MODULE_EXPORT GType                                                 \
  shm_combo_box_plugin_register_type (GTypeModule *type_module)         \
  {                                                                     \
    return object_name##_register_type (type_module);                   \
  }                                                                     \

struct _ShmComboBoxClass
{
  GObjectClass parent;

  void
  (*show) (ShmComboBox *self,
           ClutterActor *actor,
           GList *items,
           gint selected_index,
           ShmComboBoxSelectCallback callback,
           gpointer user_data);
  void
  (*hide) (ShmComboBox *self);

  /* Padding for future expansion */
  void (*_shm_reserved1) (void);
  void (*_shm_reserved2) (void);
  void (*_shm_reserved3) (void);
  void (*_shm_reserved4) (void);
};

GType
shm_combo_box_get_type (void)
G_GNUC_CONST;

#endif /* __SHM_COMBO_BOX_H_ */
