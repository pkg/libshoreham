/*
 * Copyright © 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __SHOREHAM_H__
#define __SHOREHAM_H__

#include <clutter/clutter.h>
#include <glib.h>

/**
 * ShmComboBoxSelectCallack:
 *
 * @selected : index of the selected item.
 * @user_data : (nullable) User data to be passed to the @c_handler
 *
 * Signature of the callback called when the user selects an item in a
 * combo box shown with shm_combo_show().
 */
typedef void (*ShmComboBoxSelectCallback) (gint selected,
                                           gpointer user_data);


/**
 * ShmVKeyboardConfirmtCallack:
 *
 * @user_data : (nullable) User data to be passed to the @c_handler
 *
 * Signature of the callback called when the user select submit button
 * for confirm text entry shown with shm_vkeyboard_show().
 *
 */
typedef void (*ShmVKeyboardConfirmCallback) (gpointer user_data);

G_DEPRECATED_FOR(shm_combo_box_show_for_actor)
void
shm_combo_box_show (GList *items,
                    gint selected_index,
                    ShmComboBoxSelectCallback callback,
                    gpointer user_data);
void
shm_combo_box_show_for_actor (ClutterActor *actor,
                              GList *items,
                              gint selected_index,
                              ShmComboBoxSelectCallback callback,
                              gpointer user_data);
void
shm_combo_box_hide (void);

G_DEPRECATED_FOR(shm_vkeyboard_show_for_actor)
void
shm_vkeyboard_show (const gchar *default_text,
                    ShmVKeyboardConfirmCallback callback,
                    gpointer user_data);
void
shm_vkeyboard_show_for_actor (ClutterActor *actor,
                              const gchar *default_text,
                              ShmVKeyboardConfirmCallback callback,
                              gpointer user_data);
void
shm_vkeyboard_hide (void);
#endif
